package com.example.test.ui.main;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class TFTApi {

    private final String BASE_URL = "https://apitft.herokuapp.com";

    ArrayList<Champion> getChampions() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("api")
                .appendPath("tft")
                .appendPath("champions")
                .build();
        String url = builtUri.toString();

        Log.d(null, url.toString());

        return doCall(url);
    }

    private ArrayList<Champion> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Champion> processJson(String jsonResponse) {
        ArrayList<Champion> champions = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonChampions = data.getJSONArray("champions");
            for (int i = 0; i < jsonChampions.length(); i++) {
                JSONObject jsonChampion = jsonChampions.getJSONObject(i);

                Champion champion = new Champion();
                champion.setName(jsonChampion.getString("name"));
                champion.setCost(jsonChampion.getInt("cost"));
                champion.setTrait_pri(jsonChampion.getString("trait_pri"));
                champion.setTrait_sec(jsonChampion.getString("trait_sec"));
                champion.setTrait_tri(jsonChampion.getString("trait_tri"));
                champion.setImageUrl(jsonChampion.getString("imageUrl"));
                champion.setProfileImageUrl(jsonChampion.getString("profileImageUrl"));
                champions.add(champion);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return champions;
    }
}
