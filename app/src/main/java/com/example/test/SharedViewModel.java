package com.example.test;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test.ui.main.Champion;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Champion> selected = new MutableLiveData<Champion>();

    public void select(Champion champion) {
        selected.setValue(champion);
    }

    public LiveData<Champion> getSelected() {
        return selected;
    }
}
